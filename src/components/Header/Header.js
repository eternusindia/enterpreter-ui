/**
 * App Header
 */
/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { Link } from 'react-router-dom';
import screenfull from 'screenfull';
import MenuIcon from 'material-ui-icons/Menu';
import $ from 'jquery';

// actions
import { collapsedSidebarAction } from '../../actions';

// components
import Notifications from './Notifications';
import ChatSidebar from '../ChatSidebar/ChatSidebar';
import DashboardOverlay from '../DashboardOverlay/DashboardOverlay';
import LanguageProvider from './LanguageProvider';
import SearchForm from './SearchForm';
import QuickLinks from './QuickLinks';
import Cart from './Cart';

// intl messages
import IntlMessages from '../../util/IntlMessages';

class Header extends Component {

  state = {
    customizer: false
  }

  // function to change the state of collapsed sidebar
  onToggleNavCollapsed = (event) => {
    const val = !this.props.collapsedSidebar;
    this.props.collapsedSidebarAction(val);
  }

  // open dashboard overlay
  openDashboardOverlay() {
    $('.dashboard-overlay').toggleClass('d-none');
    $('.dashboard-overlay').toggleClass('show');
    if ($('.dashboard-overlay').hasClass('show')) {
      $('body').css('overflow', 'hidden');
    } else {
      $('body').css('overflow', '');
    }
  }

  // close dashboard overlay
  closeDashboardOverlay() {
    $('.dashboard-overlay').removeClass('show');
    $('.dashboard-overlay').addClass('d-none');
    $('body').css('overflow', '');
  }

  // toggle screen full
  toggleScreenFull() {
    screenfull.toggle();
  }

  render() {
    $('body').click(function () {
      $('.dashboard-overlay').removeClass('show');
      $('.dashboard-overlay').addClass('d-none');
      $('body').css('overflow', '');
    });
    return (
      <AppBar position="fixed" className="rct-header">
        <Toolbar className="d-flex justify-content-between w-100">
          <ul className="list-inline mb-0 navbar-left">
            <li className="list-inline-item" onClick={(e) => this.onToggleNavCollapsed(e)}>
              <IconButton color="inherit" aria-label="Menu" className="humburger">
                <MenuIcon />
              </IconButton>
            </li>
            <QuickLinks />
            <li className="list-inline-item">
              <a href="javascript:void(0)" className="header-icon text-secondary border-secondary tour-step-2" onClick={() => this.openDashboardOverlay()}>
                <i className="ti-info"></i>
              </a>
            </li>
          </ul>
          <ul className="navbar-right list-inline">
            <li className="list-inline-item">
              <Link to="/app/pages/pricing" variant="raised" className="btn-yellow upgrade-btn tour-step-3">
                <IntlMessages id="widgets.upgrade" />
              </Link>
            </li>
            <LanguageProvider />
            <Notifications />
            <Cart />
            <li className="list-inline-item setting-icon">
              <IconButton aria-label="settings" onClick={() => this.setState({ customizer: true })}>
                <i className="ti-comment-alt"></i>
              </IconButton>
            </li>
            <li className="list-inline-item">
              <IconButton aria-label="settings" onClick={() => this.toggleScreenFull()}>
                <i className="ti-fullscreen"></i>
              </IconButton>
            </li>
            <li className="list-inline-item search-icon">
              <SearchForm />
            </li>
          </ul>
          <Drawer
            anchor={this.props.rtlLayout ? 'left' : 'right'}
            open={this.state.customizer}
            onClose={() => this.setState({ customizer: false })}>
            <ChatSidebar />
          </Drawer>
        </Toolbar>
        <DashboardOverlay
          onClose={() => this.closeDashboardOverlay()}
        />
      </AppBar>
    );
  }
}

// map state to props
const mapStateToProps = ({ settings }) => ({
  collapsedSidebar: settings.navCollapsed,
  rtlLayout: settings.rtlLayout
});

export default connect(mapStateToProps, {
  collapsedSidebarAction
})(Header);
