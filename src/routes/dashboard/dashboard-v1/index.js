/**
 * Dashboard V1
 */
/* eslint-disable */
import React, { Component } from 'react';

// Widgets
import TotalEarnsWithAreaChartWidget from '../../../components/Widgets/TotalEarnsWithAreaChart';

import TotalSalesWidget from '../../../components/Widgets/TotalSales';
import NetProfitWidget from '../../../components/Widgets/NetProfit';
import TaxStatsWidget from '../../../components/Widgets/TaxStats';
import ExpensesWidget from '../../../components/Widgets/Expenses';
import OverallTrafficStatusWidget from '../../../components/Widgets/OverallTrafficStatus';

import ToDoListWidget from '../../../components/Widgets/ToDoList';
import NewCustomersWidget from '../../../components/Widgets/NewCustomers';
import Notifications from '../../../components/Widgets/Notifications';

import OrderStatusWidget from '../../../components/Widgets/OrderStatus';

import NewEmailsWidget from '../../../components/Widgets/NewEmails';
import EmployeePayrollWidget from '../../../components/Widgets/EmployeePayroll';

import SocialFeedsWidget from '../../../components/Widgets/SocialFeeds';

// page title bar
import PageTitleBar from '../../../components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from '../../../util/IntlMessages';

// rct card box
import RctCollapsibleCard from '../../../components/RctCollapsibleCard/RctCollapsibleCard';

// dashboard data
import {
  totalEarns,
  todoData,
  newCustomers,
  messages,
  notificationTypes,
  notifications,
  ordersStatus,
  newEmails,
  employeePayroll,
  feeds,
  trafficStatus,
  totalSales,
  netProfit,
  taxStats,
  expenses
} from './data';

// Main Component
export default class DashboardOne extends Component {
  render() {
    return (
      <div className="dashboard-v1">
        <PageTitleBar title={<IntlMessages id="sidebar.dashboard" />} match={this.props.match} />
        <RctCollapsibleCard
          heading={<IntlMessages id="widgets.totalEarns" />}
          collapsible
          reloadable
          closeable
        >
          <TotalEarnsWithAreaChartWidget chartData={totalEarns} />
        </RctCollapsibleCard>
        <div className="row">
          <RctCollapsibleCard
            customClasses="to-do-list tour-step-7"
            colClasses="col-sm-6 col-md-4 col-lg-4 d-xs-half-block d-xxs-full"
            heading={<IntlMessages id="widgets.toDoList" />}
            collapsible
            reloadable
            closeable
            fullBlock
          >
            <ToDoListWidget data={todoData} />
          </RctCollapsibleCard>
          <RctCollapsibleCard
            colClasses="col-sm-6 col-md-4 col-lg-4 d-xs-half-block d-xxs-full"
            heading={<IntlMessages id="widgets.newCustomers" />}
            collapsible
            reloadable
            closeable
            fullBlock
          >
            <NewCustomersWidget data={newCustomers} />
          </RctCollapsibleCard>
          <RctCollapsibleCard
            colClasses="col-sm-12 col-md-4 col-lg-4 d-xs-full"
            fullBlock
          >
            <Notifications
              messages={messages}
              notificationTypes={notificationTypes}
              notifications={notifications}
            />
          </RctCollapsibleCard>
        </div>
        <RctCollapsibleCard
          heading={<IntlMessages id="widgets.orderStatus" />}
          collapsible
          reloadable
          closeable
          fullBlock
        >
          <OrderStatusWidget data={ordersStatus} />
        </RctCollapsibleCard>
        <div className="row">
          <RctCollapsibleCard
            colClasses="col-sm-12 col-md-7 col-xl-7 b-100"
            heading={<IntlMessages id="widgets.newEmails" />}
            collapsible
            reloadable
            closeable
            fullBlock
          >
            <NewEmailsWidget data={newEmails} />
          </RctCollapsibleCard>
          <RctCollapsibleCard
            colClasses="col-sm-12 col-md-5 col-xl-5 b-100"
            heading={<IntlMessages id="widgets.employeePayroll" />}
            collapsible
            reloadable
            closeable
            fullBlock
          >
            <EmployeePayrollWidget data={employeePayroll} />
          </RctCollapsibleCard>
        </div>
        <div className="social-card-wrapper">
          <div className="row">
            <div className="col-sm-6 col-md-3 col-lg-3 b-3 d-xxs-half-block d-xs-half-block">
              <SocialFeedsWidget
                type="facebook"
                friendsCount={feeds.facebook.friendsCount}
                icon="ti-facebook"
                feedsCount={feeds.facebook.feedsCount}
              />
            </div>
            <div className="col-sm-6 col-md-3 col-lg-3 b-3 d-xxs-half-block d-xs-half-block">
              <SocialFeedsWidget
                type="twitter"
                friendsCount={feeds.twitter.friendsCount}
                feedsCount={feeds.twitter.feedsCount}
                icon="ti-twitter"
              />
            </div>
            <div className="col-sm-6 col-md-3 col-lg-3 b-3 d-xxs-half-block d-xs-half-block">
              <SocialFeedsWidget
                type="linkedIn"
                friendsCount={feeds.linkedIn.friendsCount}
                feedsCount={feeds.linkedIn.feedsCount}
                icon="ti-linkedin"
              />
            </div>
            <div className="col-sm-6 col-md-3 col-lg-3 b-3 d-xxs-half-block d-xs-half-block">
              <SocialFeedsWidget
                type="google"
                friendsCount={feeds.google.friendsCount}
                feedsCount={feeds.google.feedsCount}
                icon="ti-google"
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-6 col-lg-6 d-xs-full">
            <div className="row">
              <div className="col-sm-6 col-md-6 d-xxs-half-block">
                <TotalSalesWidget
                  label={totalSales.label}
                  chartdata={totalSales.chartdata}
                  labels={totalSales.labels}
                />
              </div>
              <div className="col-sm-6 col-md-6 d-xxs-half-block">
                <NetProfitWidget
                  label={netProfit.label}
                  chartdata={netProfit.chartdata}
                  labels={netProfit.labels}
                />
              </div>
              <div className="col-sm-6 col-md-6 d-xxs-half-block">
                <TaxStatsWidget
                  label={taxStats.label}
                  chartdata={taxStats.chartdata}
                  labels={taxStats.labels}
                />
              </div>
              <div className="col-sm-6 col-md-6 d-xxs-half-block">
                <ExpensesWidget
                  label={expenses.label}
                  chartdata={expenses.chartdata}
                  labels={expenses.labels}
                />
              </div>
            </div>
          </div>
          <RctCollapsibleCard
            colClasses="col-sm-12 col-md-6 col-lg-6 d-xs-full"
            heading={<IntlMessages id="widgets.overallTrafficStatus" />}
            collapsible
            reloadable
            closeable
            fullBlock
          >
            <OverallTrafficStatusWidget
              chartData={trafficStatus}
            />
          </RctCollapsibleCard>
        </div>
      </div>
    );
  }
}
